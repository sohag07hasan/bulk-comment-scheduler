<?php
/*
 * plugin name: Bulk Comment Auto scheduler
 * author: Mahibul Hasan Sohag
 * author uri: http://sohag07hasan.elance.com
 * plugin uri: http://sohag07hasan.elance.com
 * Description: Bulk uploader helps to uplad numerous comments and scheduler helps to manipulate these comments at a certain interval.
 * version: 1.1.0
 */

define('BULKCOMMENTSCHUDLER_DIR', dirname(__FILE__));
define("BULKCOMMENTSCHUDLER_FILE", __FILE__);
define("BULKCOMMENTSCHUDLER_URL", plugins_url('', __FILE__));

include BULKCOMMENTSCHUDLER_DIR . '/classes/class.utility.php';
bulk_comment_utility::init();

include BULKCOMMENTSCHUDLER_DIR . '/classes/class.commentsuploader.php';
bulk_comment_uploader::init();

include BULKCOMMENTSCHUDLER_DIR . '/classes/class.metabox.php';
bulk_comment_metabox::init();
