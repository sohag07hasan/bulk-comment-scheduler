<?php
/*
 * Comment uploader
 */

class bulk_comment_uploader{
	
	static $log = array();
	
	/*
	 * Binding all hooks
	 */
	static function init(){
		add_action('admin_menu', array(get_class(), 'mange_admin_menu'));
		add_action('init', array(get_class(), 'upload_handler'));
		register_activation_hook(BULKCOMMENTSCHUDLER_FILE, array(get_class(), 'create_tables'));
		add_action('admin_enqueue_scripts', array(get_class(), 'adding_css_js'), 20);
	}
	
	/*
	 * adding css and js
	 * */
	static function adding_css_js(){
		wp_enqueue_script('jquery');
		wp_register_script('bulk-comments-manipulations-js', BULKCOMMENTSCHUDLER_URL . '/js/bulk-comments.js');
		wp_enqueue_script('bulk-comments-manipulations-js');
		wp_localize_script('bulk-comments-manipulations-js', 'BulkComments', array('ajaxurl'=>admin_url( 'admin-ajax.php' )));
		
		wp_register_style('bulk-comments-manipulations-css', BULKCOMMENTSCHUDLER_URL . '/css/bulk-comments.css');
		wp_enqueue_style('bulk-comments-manipulations-css');
	}
	 
	
	//comment submenu
	static function mange_admin_menu(){
		add_comments_page('bulk comment management', 'Bulk Comments', 'manage_options', 'bulk_comment_management', array(get_class(), 'master_names_uploader'));
	}
	
	//comment uploader
	static function master_names_uploader(){
		include dirname(__FILE__) . '/includes/uploader-form.php';
	}
	
	/*
	 * handles upload
	 */
	static function upload_handler(){
		
		//name
		if($_POST['comment-name-submitted'] == 'Y'){
			//checking if the comment file is uploaded
			 if (empty($_FILES['bulknames']['tmp_name'])) {
				self::$log['error'][] = 'No file uploaded, aborting....';			
				return;
			}
			if(self::is_csv($_FILES['bulknames'])){
				return self::names_handler();
			}
			
			self::$log['error'][] = 'uploaded file is not a csv, aborting....';			
			return;
		}
	}
	
	
	/*
	 * checking if the file has an .csv extention
	 */
	static function is_csv($file = array()){
				
		return (strpos($file['name'], 'csv')) ? true : false;
	}
	
	
	/*
	 * upload comments
	 * triger errors if any
	 */
	static function comments_handler($post_id){
		if(!class_exists('File_CSV_DataSource')){
			include dirname(__FILE__) . '/class.csv-importer.php';
		}
		$time_start = microtime(true);
		$csv = new File_CSV_DataSource();
		$file = $_FILES['bulkcomments']['tmp_name'];
		self::stripBOM($file);

		//now loading file
		if (!$csv->load($file)) {
			self::$log['error'][] = 'Failed to load file, aborting...';
			return;
		}

		//uploading started
		$skipped = 0;
		$imported = 0;
		$csv->symmetrize();
		foreach ($csv->getRawArray() as $csv_data) {
			if(self::create_comment($csv_data, $post_id)){
				$imported ++;
			}
			else{
				$skipped ++;
			}
		}
		
		$exec_time = microtime(true) - $time_start;
		if ($skipped) {
			self::$log['notice'][] = "<b>Skipped {$skipped} comments (most likely due to empty content.</b>";
		}
		self::$log['notice'][] = sprintf("<b>Imported {$imported} comments in %.2f seconds.</b>", $exec_time);
	}
	
	/*
	 * upload names
	 * triger errors if any
	 */
	static function names_handler($post_id = null){
		if(!class_exists('File_CSV_DataSource')){
			include dirname(__FILE__) . '/class.csv-importer.php';
		}
		$time_start = microtime(true);
		$csv = new File_CSV_DataSource();
		$file = $_FILES['bulknames']['tmp_name'];
		self::stripBOM($file);

		//now loading file
		if (!$csv->load($file)) {
			self::$log['error'][] = 'Failed to load file, aborting...';
			return;
		}

		//uploading started
		$skipped = 0;
		$imported = 0;
		$csv->symmetrize();
		foreach ($csv->getRawArray() as $csv_data) {
					
			if(self::create_name($csv_data)){
				$imported ++;
			}
			else{
				$skipped ++;
			}
		
		}

		$exec_time = microtime(true) - $time_start;
		if ($skipped) {
			self::$log['notice'][] = "<b>Skipped {$skipped}names (most likely due to empty content).</b>";
		}
		self::$log['notice'][] = sprintf("<b>Imported {$imported} names in %.2f seconds.</b>", $exec_time);
	}
	
	
	
	//database manipulate
	static function create_tables(){
		global $wpdb;
		$tables = self::get_tables_name();
		extract($tables);
		
		$sql_1 = "CREATE TABLE IF NOT EXISTS $comments(
				`id` bigint unsigned NOT NULL AUTO_INCREMENT,
				`post_id` bigint unsigned NOT NULL,
				`content` longtext NOT NULL,
				 PRIMARY KEY(id)				 			
			)";
			
		$sql_2 = "CREATE TABLE IF NOT EXISTS $names(
			       `id` bigint unsigned NOT NULL AUTO_INCREMENT,
			       `name` varchar(100) NOT NULL,			      
			        PRIMARY KEY(id)
			     
		)";
		
		$sql_3 = "CREATE TABLE IF NOT EXISTS $scheduler(				
				`post_id` bigint unsigned NOT NULL,
				`last_imported` bigint unsigned NOT NULL,
				`interval` bigint unsigned NOT NULL,
				`varience` bigint unsigned NOT NULL				 			 			
			)";
		
		$sql_4 = "CREATE TABLE IF NOT EXISTS $sp_names(
				`id` bigint unsigned NOT NULL AUTO_INCREMENT,
				`post_id` bigint unsigned NOT NULL,
				`name` longtext NOT NULL,
				 PRIMARY KEY(id)				 			
			)";
		
		if(!function_exists('dbDelta')) :
				include ABSPATH . 'wp-admin/includes/upgrade.php';
		endif;
			
		dbDelta($sql_1);
		dbDelta($sql_2); 
		dbDelta($sql_3); 
		dbDelta($sql_4); 
	}
	
	//returns the table names
	static function get_tables_name(){
		global $wpdb;
		return array(
			'comments' => $wpdb->prefix . 'bulk_comments',
			'names' => $wpdb->prefix . 'bulk_names',
			'scheduler' => $wpdb->prefix . 'bulk_scheduler',
			'sp_names' => $wpdb->prefix . 'bulk_sp_names'
		);
	}
	
	
	//print the log message
	static function print_messages(){
		if (!empty(self::$log['error'])):
		?>
			<div class="error">

				<?php foreach (self::$log['error'] as $error): ?>
					<p><?php echo $error; ?></p>
				<?php endforeach; ?>

			</div>
		<?php
		endif;
		
		if (!empty(self::$log['notice'])):
		?>
			<div class="updated fade">

				<?php foreach (self::$log['notice'] as $notice): ?>
					<p><?php echo $notice; ?></p>
				<?php endforeach; ?>

			</div>
		<?php
		endif;
	}
	
	//stip boom
	static function stripBOM($fname){
		 $res = fopen($fname, 'rb');
		if (false !== $res) {
			$bytes = fread($res, 3);
			if ($bytes == pack('CCC', 0xef, 0xbb, 0xbf)) {
				self::$log['notice'][] = 'Getting rid of byte order mark...';
				fclose($res);

				$contents = file_get_contents($fname);
				if (false === $contents) {
					trigger_error('Failed to get file contents.', E_USER_WARNING);
				}
				$contents = substr($contents, 3);
				$success = file_put_contents($fname, $contents);
				if (false === $success) {
					trigger_error('Failed to put file contents.', E_USER_WARNING);
				}
			} else {
				fclose($res);
			}
		} 
		else {
			slef::$log['error'][] = 'Failed to open file, aborting.';
		}
		
	}
	
	
	/*
	 * create comments
	 */
	static function create_comment($data, $post_id){
				
		$tables = self::get_tables_name();
		extract($tables);
		global $wpdb;
		if(isset($data[0]) && !empty($data[0])){
			$id = $wpdb->insert($comments, array('post_id'=>$post_id, 'content'=>$data[0]), array('%d', '%s'));
			
		}
		else{
			$id = false;
		}
		return $id;
		
	}
	
	/*
	 * create names
	 */
	static function create_name($data){
		//var_dump($data);
		$tables = self::get_tables_name();
		extract($tables);
		global $wpdb;
		if(isset($data[0]) && !empty($data[0])){
			$id = $wpdb->insert($names, array('name'=>$data[0]), array('%s'));
			
		}
		else{
			$id = false;
		}
		return $id;
		
	}
	
	static function create_specificname($data, $post_id){
		$tables = self::get_tables_name();
		extract($tables);
		global $wpdb;
		if(isset($data[0]) && !empty($data[0])){
			$wpdb->insert($sp_names, array('post_id'=>$post_id, 'name'=>$data[0]), array('%d','%s'));		
		}
	}
	
	static function specific_name_handler($post_id){
		if(!class_exists('File_CSV_DataSource')){
			include dirname(__FILE__) . '/class.csv-importer.php';
		}
		
		$csv = new File_CSV_DataSource();
		$file = $_FILES['bulknames']['tmp_name'];
		self::stripBOM($file);

		//now loading file
		if (!$csv->load($file)) {			
			return;
		}
		
		$csv->symmetrize();
		foreach ($csv->getRawArray() as $csv_data) {
			self::create_specificname($csv_data, $post_id);
		}
		
		
	}
}
