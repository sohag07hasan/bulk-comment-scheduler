<div class="wrap">
	
	<input type="hidden" name="upload_post_id" value="<?php echo $post->ID; ?>" />
	
	<h2>Schedule the Bulk Comments </h2>
	<p>Enable Bulk Comments posting <input type="checkbox"  <?php checked('Y', $scheduler_info['enable']) ?>  name="enable_bulk_comments" value="Y" /></p>
	<table class="form-table">
		
		<tr>
		<td>CRON SCRIPT</td>
		<td colspan="4">			
			<input size="60" type="text" readonly value="<?php echo 'wget ' . BULKCOMMENTSCHUDLER_URL . '/cron/scheduler.php' ?>" /> <br/>
			Use this whole string in server cron tag with a suitable scheduling and this url can be used from any capnel around the world
		</td>
		</tr>
		
		<tr>
			<td>SCHEDULER :</td>
			<td>DAY <?php echo bulk_comment_utility::get_day_interval($scheduler_info['day']); ?></td>
			<td>HOUR <?php echo bulk_comment_utility::get_hour_interval($scheduler_info['hour']); ?></td>
			<td>MINUTES <?php echo bulk_comment_utility::get_Original_minutes_interval($scheduler_info['min']);?> </td>
			<td>VARIANCE (Min) <?php  echo bulk_comment_utility::get_minutes_interval($scheduler_info['varience']) ?> </td>
		</tr>
	</table>
	
	<hr/>
	
	<p>Use Master Name <input type="checkbox"  <?php checked('Y', $scheduler_info['en-name']) ?>  name="enable_master_names" value="Y" /></p>
	<p>Or you can upload a csv file for names and these will be only used for this post</p>
	<p> Total Names associated for this post: <?php echo self::get_names_count($post->ID); ?> </p>
	<p>Upload a csv file (names) <input type="file" name="bulknames" /> </p>
		
	
</div>
