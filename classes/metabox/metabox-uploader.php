<div class="wrap">
	
	<h2>Import the Bulk Comments and Names </h2>
				
	<table class="form-table">
		<tr>
			<td>Comments Statistics </td>
			<td> Live : <?php echo $bulk_statistics['imported']; ?> </td>
			<td> Remaining : <span id="bulk-comment-remaining"><?php echo $bulk_statistics['remaining']; ?></span> </td>
		</tr>
		<tr>
			<td>Upload a csv file (comments)</td>
			<td> <input type="file" name="bulkcomments" /> </td>
		</tr>	
		
	</table>
</div>
