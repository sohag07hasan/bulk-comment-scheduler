<div class="wrap">
	<h2>Upload your comment to the Schedule Manually</h2>
	
	<div id="comment-ajax-loader"  style="text-align: center; display: none"><img  src="<?php echo $image;?>" /></div>
	<input type="hidden" id="uploader-post-id" value="<?php echo $post->ID; ?>" />
	<div id="comments-message" style="display:none"></div>
	<table class="form-table" id="manual-comment-uploader-table">
		<tr>
			<td>
				<textarea rows="3" cols="88" id="manual-comment-uploader"></textarea>
			</td>			
		</tr>
		<tr>
			<td>
				<input type="button" value="add to the scheduler" id="add-to-the-scheduler" />
			</td>
		</tr>
	</table>
	
</div>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#add-to-the-scheduler').bind('click', function(){
			
			$('#comments-message').hide();
			$('#comments-message').attr('class', '');
			$('#comments-message').html(null);
			$('#manual-comment-uploader-table').hide();
			$('#comment-ajax-loader').show();
			var comment = $('#manual-comment-uploader').val();
			$.ajax({						
				async: false,
				type:'post',			
				dataType:"html",
				url:BulkComments.ajaxurl,
				cache:false,
				timeout:10000,
				data:{
					'action' : 'manual_comments',
					'comment' : comment,
					'post_id' : $('#uploader-post-id').val()
				},

				success:function(result){				
					if(result == 1){
						var confirmtext = "<p>Comments Field is empty</p>";
						$('#comments-message').attr('class', 'error-message');
					}
					else if(result == 2){
						var confirmtext = "<p>Comment uploaded successfully</p>";
						$('#manual-comment-uploader').val(null);
						$('#comments-message').attr('class', 'successful-message');
						var count = $('#bulk-comment-remaining').html();
						count ++;
						$('#bulk-comment-remaining').html(count);
					}
					else{
						var confirmtext = "<p>Comment cannot beuploaded! Please try again</p>";
						$('#comments-message').attr('class', 'error-message');
					}
					
					$('#comments-message').html(confirmtext);
					$('#comment-ajax-loader').hide();
					$('#comments-message').show();
					$('#manual-comment-uploader-table').show();
				},

				error: function(jqXHR, textStatus, errorThrown){
					var confirmtext = "<p>Comment cannot be uploaded! Please try again</p>";
					$('#comments-message').html(confirmtext);
					$('#comments-message').attr('class', 'error-message');
					$('#comment-ajax-loader').hide();
					$('#comments-message').show();
					$('#manual-comment-uploader-table').show();
				}

		});	
		});
	});
</script>