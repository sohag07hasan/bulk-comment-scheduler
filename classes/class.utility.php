<?php
/*
 * It has some helper functions to sanitize content and wraper for the offline scheduling
 */

class bulk_comment_utility{
	
	static $hours = 24;
	static $days = 30;
	static $varience = 60;
	static $mins = 60;
	
	
	//initfunctin
	static function init(){
		add_action('deleted_post', array(get_class(), 'delete_scheduled_post'));
	}
	
	//when the post is deleted the function renders
	static function delete_scheduled_post($post_id){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		
		$wpdb->query("DELETE FROM $comments WHERE post_id = '$post_id'");
		$wpdb->query("DELETE FROM $scheduler WHERE post_id = '$post_id'");
		$wpdb->query("DELETE FROM $sp_names WHERE post_id = '$post_id'");
	}
	
	
	
	/*
	 * get an hour interval of 23 hours
	 */
	static function get_hour_interval($value){
		$select = '<select name="bulk_hour_interval">';
		
			for($i=0; $i<self::$hours; $i++){
				//$select .= '<option '.selected($i, $value).' value="'.$i.'">'.$i.'</option>';
				$select .= "<option value='$i' ".selected($i, $value, false)."> $i </option>";
			}
		$select .= '</select>';
		
		return $select;
	}
	
	/*
	 * get an day interval of 23 hours
	 */
	static function get_day_interval($value){
		$select = '<select name="bulk_day_interval">';
		
			for($i=0; $i<self::$days; $i++){
				//$select .= '<option '.selected($i, $value).' value="'.$i.'">'.$i.'</option>';
				$select .= "<option value='$i' ".selected($i, $value, false)."> $i </option>";
			}
		$select .= '</select>';
		
		return $select;
	}
	
	/*
	 * get minute interval 59 minutes hours
	 */
	static function get_Original_minutes_interval($value){
		$select = '<select name="bulk_min_interval">';
		
			for($i=0; $i<self::$mins; $i++){
				//$select .= '<option '.selected($i, $value).' value="'.$i.'">'.$i.'</option>';
				$select .= "<option value='$i' ".selected($i, $value, false)."> $i </option>";
			}
		$select .= '</select>';
		
		return $select;
	}
	
	//get the varience 
	static function get_minutes_interval($value){
		$select = '<select name="varience">';
		for($i=0; $i<self::$varience; $i++){
			$select .= '<option '.selected($i, $value, false).' value="'.$i.'">+- '.$i.'</option>';
		}
		$select .= '</select>';
		return $select;
	}


	//process the cron functionality
	static function process_schedule(){
		$scheduled_posts = self::get_scheduled_posts();
		if(empty($scheduled_posts)) return;
				
		$time = current_time('timestamp');
		foreach($scheduled_posts as $post){
			
			if(self::has_comment($post)) : 			
				$interval = $post->interval * 60 + self::varience($post->varience) * 60;
				$prevtime = $post->last_imported + $interval;
				if($prevtime < $time || $post->last_imported == 0){
					self::comment_manipulation($post);
				}			
						
			endif;
		}
		
	}
	
	//manipulates the comments
	static function comment_manipulation($post){
		$name = self::get_commenter_name($post);
		$comment = self::get_bulk_comment($post);
		$data = array(
			'comment_post_ID' => $post->post_id,
			'comment_author' => $name,
			'comment_content' => $comment->content,
			'comment_approved' => 1,
			
		);
		
		
		if(wp_insert_comment($data)){
			wp_update_comment_count($post->post_id);
			
			self::update_the_schedule($post);
			self::update_live_count($post);
			
			self::remove_the_comment($comment);
		}
	}
	
	//update the schedule
	static function update_the_schedule($post){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		$wpdb->update($scheduler, array('last_imported'=>current_time('timestamp')), array('post_id'=>$post->post_id), array('%d'), array('%d'));
	}
	
	
	//update comment count
	static function update_live_count($post){
		$imported = get_post_meta($post->post_id, 'imported_bulk_comments', true);
		if($imported){
			$imported ++;
		}
		else{
			$imported = 1;
		}
		update_post_meta($post->post_id, 'imported_bulk_comments', $imported);
	}
	
	//return the commetner name
	static function get_commenter_name($post){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		
		if(self::master_name_enalbed($post)){
			$name = $wpdb->get_row("SELECT * FROM $names ORDER BY RAND() LIMIT 1");
			
		}
		else{
			$name = $wpdb->get_row("SELECT * FROM $sp_names WHERE post_id = '$post->post_id' ORDER BY RAND() LIMIT 1");
			
		}
		
		return $name->name;
	}
	
	static function master_name_enalbed($post){
		return (get_post_meta($post->post_id, 'enable_master_names', true) == "Y") ? true : false;
	}
	
	
	//varience setting
	static function varience($minutes){
		if($minutes){
			$min = 0 - $minutes;
			$max = (int) $max;
			return rand($min, $max);
		}
		else{
			return 0;
		}
	}
	
	//get the scheduled posts
	static function get_scheduled_posts(){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		return $wpdb->get_results("SELECT * FROM $scheduler");
		
	}
	
	
	//get comment content
	static function get_bulk_comment($post){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		//return $wpdb->get_row("SELECT * FROM $comments WHERE post_id = '$post->post_id'");
		return $wpdb->get_row("SELECT * FROM $comments WHERE post_id = '$post->post_id' ORDER BY id LIMIT 1");
	}
	
	//remove the delteed comment
	static function remove_the_comment($comment){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		$wpdb->query("DELETE FROM $comments WHERE id = '$comment->id'");
	}
	
	/*
	 * check if a scheduled post has comments
	 * */
	static function has_comment($post){
		global $wpdb;
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		
		$remaining = $wpdb->get_var("SELECT COUNT(id) FROM $comments WHERE post_id = '$post->post_id'");
		
		return ($remaining) ? true : false;
	}
		
	
}
