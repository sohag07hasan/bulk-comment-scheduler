<?php

/*
 * creates metabox and manipulate the metabox
 */
class bulk_comment_metabox{
	
//initialize the hooks
	static function init(){
		add_action( 'add_meta_boxes', array(get_class(), 'meta_boxes' ));
		add_action('save_post', array(get_class(), 'save_post_meta'), 100, 2);
		
		//add_action('admin_enqueue_scripts',array(get_class(),'adding_css_js'),20);
		add_action('wp_ajax_manual_comments', array(get_class(), 'insert_manaul_comment'));
		add_action('wp_ajax_nopriv_manual_comments', array(get_class(), 'insert_manaul_comment'));
	}	
	
	/*
	 * add meta boxes
	 */
	static function meta_boxes(){
		$post_types=get_post_types();
		foreach($post_types as $post_type){
			if($post_type == 'page') continue;
			add_meta_box( 'bulk_comment_metabox-scheduler', 'Bulk Comment Schedular', array(get_class(), 'meta_box_scheduler'), $post_type, 'advanced', 'high');
			add_meta_box( 'bulk_comment_metabox-uploader', 'Bulk Comment Uploader', array(get_class(), 'meta_box_content_uploader'), $post_type, 'advanced', 'high');
			add_meta_box( 'single_comment_metabox-uploader', 'Manual Comment Addition', array(get_class(), 'meta_box_for_manualcomment'), $post_type, 'advanced', 'high');
		}
	}
	
	//metabox content
	static function meta_box_scheduler(){
		global $post;
		$scheduler_info = self::get_scheduler_info($post->ID);
		//var_dump($scheduler_info);	
		include dirname(__FILE__) . '/metabox/metabox-scheduler.php';
	}
	
	//metabox uploader
	static function meta_box_content_uploader(){
		global $post;
		
		$bulk_statistics = self::get_statistics($post->ID);
		//var_dump($bulk_statistics);												
		include dirname(__FILE__) . '/metabox/metabox-uploader.php';
	}	
	
	/*
	 * manually upload the comment to the scheduler
	 */
	static function meta_box_for_manualcomment(){
		global $post;
		$image = BULKCOMMENTSCHUDLER_URL . '/images/ajax-loader.gif';
		include dirname(__FILE__) . '/metabox/manual-comment-uploader.php';
	}
	
	
	//save the post meta data
	static function save_post_meta($post_ID, $post){
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
		
		//bulk comments uploader
		if($_POST['upload_post_id'] == $post_ID):
			self::upload_handler($post_ID);
			
			if($_POST['enable_bulk_comments'] == 'Y'):
				if($_POST['bulk_day_interval']==0 && $_POST['bulk_hour_interval']==0 && $_POST['bulk_min_interval']==0) return;
				self::set_meta_and_scheduler($post_ID);			
			else :
				self::unset_meta_and_scheduler($post_ID);
			endif;
		endif;
	}
	
	static function set_meta_and_scheduler($post_ID){
		
		update_post_meta($post_ID, 'enable_bulk_comments' , $_POST['enable_bulk_comments']);
		update_post_meta($post_ID, 'bulk_day_interval' , $_POST['bulk_day_interval']);
		update_post_meta($post_ID, 'bulk_hour_interval' , $_POST['bulk_hour_interval']);
		update_post_meta($post_ID, 'bulk_min_interval' , $_POST['bulk_min_interval']);
		
		if($_POST['enable_master_names'] == 'Y'){
			update_post_meta($post_ID, 'enable_master_names' , $_POST['enable_master_names']);
		}
		else{
			delete_post_meta($post_ID, 'enable_master_names');
		}
		if(!empty($_POST['varience'])){
			update_post_meta($post_ID, 'varience' , trim($_POST['varience']));
		}
		
		self::set_offline_schedule($post_ID);
	}
	
	
	//save offline schedule
	static function set_offline_schedule($post_ID){
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		global $wpdb;
		
		$interval = ($_POST['bulk_day_interval'] * 24 + $_POST['bulk_hour_interval']) * 60 + $_POST['bulk_min_interval'];
		
		
		if(self::scheduler_exists($scheduler, $post_ID)){			
			$wpdb->update($scheduler, array('interval'=>(int)$interval, 'varience'=>(int)$_POST['varience']), array('post_id'=>$post_ID), array('%d', '%d'), array('%d'));
		}
		else{
			$wpdb->insert($scheduler, array('post_id'=>(int)$post_ID, 'interval'=>(int)$interval, 'varience'=>(int)$_POST['varience']), array('%d', '%d', '%d'));
		}
	}
	
	//unsetting all the scheduling
	static function unset_meta_and_scheduler($post_ID){
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		global $wpdb;
		$wpdb->query("DELETE FROM $scheduler WHERE post_id = '$post_ID'");
		delete_post_meta($post_ID, 'enable_bulk_comments');
	}
	
	
	//check if the post is alreay scheduled
	static function scheduler_exists($table, $id){
		global $wpdb;
		return $wpdb->get_row("SELECT * FROM $table WHERE post_id = '$id'");
	}
	
	
	static function get_scheduler_info($post_ID){
		return array(
			'enable' => get_post_meta($post_ID, 'enable_bulk_comments', true),
			'day' => get_post_meta($post_ID, 'bulk_day_interval', true),
			'hour' => get_post_meta($post_ID, 'bulk_hour_interval', true),
			'varience' => get_post_meta($post_ID, 'varience', true),
			'en-name' => get_post_meta($post_ID, 'enable_master_names', true),
			'min' => get_post_meta($post_ID, 'bulk_min_interval', true),
		);
	}
	
	//upload handler
	static function upload_handler($post_ID){
	//checking if the comment file is uploaded
		if (!empty($_FILES['bulkcomments']['tmp_name'])) {			
			if(bulk_comment_uploader::is_csv($_FILES['bulkcomments'])){
				bulk_comment_uploader::comments_handler($post_ID);
			}		
		}
		
		if (!empty($_FILES['bulknames']['tmp_name'])) {			
			if(bulk_comment_uploader::is_csv($_FILES['bulknames'])){
				bulk_comment_uploader::specific_name_handler($post_ID);
			}		
		}
		
	}
	
	//statistics of bulk comments
	static function get_statistics($post_id){
		$tables = bulk_comment_uploader::get_tables_name();
		extract($tables);
		global $wpdb;
		$remaining = $wpdb->get_var("SELECT COUNT(id) FROM $comments WHERE post_id = '$post_id'");
		$imported = get_post_meta($post_id, 'imported_bulk_comments', true);
		return array(
			'remaining' => ($remaining) ? $remaining : 0,
			'imported' => ($imported) ? $imported : 0
		);
	}
	
	/*
	 * ajax handling
	 */
	static function insert_manaul_comment(){
		$post_id = $_POST['post_id'];
		$comment = $_POST['comment'];
		if(empty($comment) || $comment==''){
			echo 1;
		}
		else{
			$tables = bulk_comment_uploader::get_tables_name();
			extract($tables);
			global $wpdb;
			$id = $wpdb->insert($comments, array('post_id'=>(int)$post_id, 'content'=>$comment), array('%d', '%s'));
			if($id){
				echo 2;
			}
			else{
				echo 3;
			}
		}
		
		exit;
	}
	
	static function get_names_count($post_id){
		$tables = bulk_comment_uploader::get_tables_name();
			extract($tables);
			global $wpdb;
			$remaining = $wpdb->get_var("SELECT COUNT(id) FROM $sp_names WHERE post_id = '$post_id'");
			
			return ($remaining) ? $remaining : 0;
	}
}
