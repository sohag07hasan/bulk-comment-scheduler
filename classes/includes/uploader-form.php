<div class="wrap">
	<h2>Master Names Uploader</h2>
		
	<h3>Name section (only .csv file accepted)</h3>
	
	<?php
		if($_POST['comment-name-submitted'] == 'Y'){
			self::print_messages();
		}
	?>
	
	<form action="" method="post" enctype="multipart/form-data">
		<input type='hidden' name="comment-name-submitted" value="Y" />
		<input type="file" name="bulknames" /> 
		<input type="submit" value="import" />
	</form>
	
</div>
